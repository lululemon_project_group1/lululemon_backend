import {Factory, Seeder} from "typeorm-seeding";
import {Connection} from "typeorm";
import {PaymentType} from "../../entity/PaymentType";
import {getRepository} from "typeorm";

// Credit/Debit, Paypal, Stripe

export class PaymentTypeSeed implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<void> {

        const repo = getRepository(PaymentType)

        let creditDebit = new PaymentType()
        creditDebit.type = 'Credit/Debit'
        await repo.save(creditDebit)

        let paypal = new PaymentType()
        paypal.type = 'Paypal'
        await repo.save(paypal)

        let stripe = new PaymentType()
        stripe.type = 'Stripe'
        await repo.save(stripe)
    }
}