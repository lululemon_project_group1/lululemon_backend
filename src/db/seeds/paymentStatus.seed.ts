import {Factory, Seeder} from "typeorm-seeding";
import {Connection} from "typeorm";
import {PaymentStatus} from "../../entity/PaymentStatus";
import {getRepository} from "typeorm";

//  Pending Payment, Accepted, Declined, Pending Refund, Refunded, Cancelled Payment, Completed

export class PaymentStatusSeed implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<void> {

        const repo = getRepository(PaymentStatus)

        let pendingPayment = new PaymentStatus()
        pendingPayment.status = 'Pending Payment'
        await repo.save(pendingPayment)

        let accepted = new PaymentStatus()
        accepted.status = 'Accepted'
        await repo.save(accepted)

        let declined = new PaymentStatus()
        declined.status = 'Declined'
        await repo.save(declined)

        let pendingRefund = new PaymentStatus()
        pendingRefund.status = 'Pending Refund'
        await repo.save(pendingRefund)

        let refunded = new PaymentStatus()
        refunded.status = 'Refunded'
        await repo.save(refunded)

        let cancelledPayment = new PaymentStatus()
        cancelledPayment.status = 'Cancelled Payment'
        await repo.save(cancelledPayment)

        let completed = new PaymentStatus()
        completed.status = 'Completed'
        await repo.save(completed)
    }
}