import {Factory, Seeder} from "typeorm-seeding";
import {Connection} from "typeorm";
import {OrderStatus} from "../../entity/OrderStatus";
import {getRepository} from "typeorm";

//  Pending Payment, Shipped, Delivered, Pending Refund, Refunded, Closed

export class OrderStatusSeed implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<void> {

        const repo = getRepository(OrderStatus)

        let pendingPayment = new OrderStatus()
        pendingPayment.status = 'Pending Payment'
        await repo.save(pendingPayment)

        let shipped = new OrderStatus()
        shipped.status = 'Shipped'
        await repo.save(shipped)

        let delivered = new OrderStatus()
        delivered.status = 'Delivered'
        await repo.save(delivered)

        let pendingRefund = new OrderStatus()
        pendingRefund.status = 'Pending Refund'
        await repo.save(pendingRefund)

        let refunded = new OrderStatus()
        refunded.status = 'Refunded'
        await repo.save(refunded)

        let closed = new OrderStatus()
        closed.status = 'Closed'
        await repo.save(closed)
    }
}