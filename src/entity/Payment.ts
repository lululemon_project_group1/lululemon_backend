import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToOne, ManyToMany, JoinTable, OneToOne, JoinColumn
} from "typeorm";
import {IsEmail, Length, Max, Min} from "class-validator";
import {Order} from "./Order";
import {OrderStatus} from "./OrderStatus";
import {PaymentStatus} from "./PaymentStatus";
import {PaymentType} from "./PaymentType";

// annotation
@Entity()
export class Payment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('decimal', {precision: 5, scale: 2})
    @Min(0)
    total: number

    @Column({nullable: true, default: false})
    isActive: boolean;

    @Column({nullable: true, default: false})
    isDelete: boolean;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(() => PaymentStatus, {nullable: false})
    paymentStatus: PaymentStatus

    @OneToOne(() => PaymentType, {nullable: false})
    paymentType: PaymentType

    @OneToOne(() => Order, order => order.payment, {cascade: true})
    // @JoinTable()
    @JoinColumn()
    order: Order
}







