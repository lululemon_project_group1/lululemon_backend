import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToOne, ManyToMany, JoinTable, OneToOne
} from "typeorm";
import {IsEmail, Length, Max, Min} from "class-validator";
import {Order} from "./Order";
import {Payment} from "./Payment";

// annotation
@Entity()
export class PaymentStatus {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    status: string

}







