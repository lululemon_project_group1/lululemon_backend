import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToOne, ManyToMany, JoinTable
} from "typeorm";
import {IsEmail, Length, Max, Min} from "class-validator";
import {Order} from "./Order";

@Entity()
export class OrderStatus {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    status: string
}
