import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne, JoinTable, JoinColumn, OneToOne
} from "typeorm";
import {Min} from "class-validator";
import {OrderStatus} from "./OrderStatus";
import {Payment} from "./Payment";

@Entity()
export class Order {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    orderItems: [{string}]

    @Column('decimal', {precision: 5, scale: 2})
    @Min(0)
    price: number

    @Column('decimal', {precision: 5, scale: 2, default: 1.13})
    @Min(1)
    taxRate: number

    @Column('decimal', {precision: 5, scale: 2})
    @Min(0)
    total: number

    // @Column()
    // shippingAddr: string
    //
    // @Column()
    // billingAddr: string

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(() => OrderStatus, {nullable: false})
    orderStatus: OrderStatus

    @OneToOne(() => Payment, payment => payment.order)

    payment: Payment

    @Column({nullable: true, default: true})
    isActive: boolean;

    @Column({nullable: true, default: false})
    isDelete: boolean;
}