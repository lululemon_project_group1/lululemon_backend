import {Router} from "express";
import {PaymentController} from "../controller/PaymentController";

const router = Router()

router.get('/', PaymentController.all)    // get all requests
router.get('/:paymentId', PaymentController.one)    // get one request
router.post('/', PaymentController.create)  // create request
router.put('/:paymentId', PaymentController.update)    // update request
router.delete('/:paymentId', PaymentController.delete) // remove request

export default router