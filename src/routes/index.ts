import {Router} from "express";
import order from "./order"
import payment from "./payment"

const routes = Router()

routes.use('/order', order)
routes.use('/payment', payment)

export default routes