import {Router} from "express";
import {OrderController} from "../controller/OrderController";

const router = Router()

router.get('/', OrderController.all)    // get all requests
router.get('/:orderId', OrderController.one)    // get one request
router.post('/', OrderController.create)  // create request
router.put('/:orderId', OrderController.update)    // update request
router.delete('/:orderId', OrderController.delete) // remove request

export default router