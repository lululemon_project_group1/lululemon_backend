export interface IdCheckRes {
    index: number,
    entities: any[]
}

export class IdCheckController {
    public static async checkIdExists(ids: number[], repo: any): Promise<IdCheckRes> {
        let index = 0
        let entities = []
        let res: IdCheckRes = {index: -1, entities}

        for (index = 0; index < ids.length; index++) {
            try {
                // map to check if corresponding id exists, add to arr of entities
                let entity = await repo.findOneOrFail(ids[index])
                res.entities.push(entity)
            } catch (e) {
                break
            }
        }

        // if previous looping is successful, index will be -1,
        // else return the index that caused the error
        if (index === ids.length) {
            res.index = -1
        } else {
            // save the index that cause the break
            res.index = ids[index]
        }

        return res
    }
}