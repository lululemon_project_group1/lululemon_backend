import {getConnection, getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Order} from "../entity/Order";
import {Err, ErrStr, HttpCode} from "../helper/Err";
import {validate} from "class-validator";
import {IdCheckController, IdCheckRes} from "./IdCheckController";
import {PaymentController} from "./PaymentController";
import {OrderStatus} from "../entity/OrderStatus";
import {Payment} from "../entity/Payment";
import {PaymentStatus} from "../entity/PaymentStatus";
import {PaymentType} from "../entity/PaymentType";

export class OrderController extends IdCheckController {

    public static get repo() {
        return getRepository(Order)
    }


    static async all(request: Request, response: Response, next: NextFunction) {
        let orders = []
        // console.log(request.params)
        try {
            orders = await OrderController.repo.find()
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, orders))
    }

    static async one(request: Request, response: Response, next: NextFunction) {
        const {orderId} = request.params
        //
        if (!orderId) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter))
        }

        let order = null

        try {
            // find one with corresponding orderId or fail
            order = await OrderController.repo.findOneOrFail(orderId)
        } catch (e) {
            // console.log(request.params)
            // console.log('error, write to db ', e)
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, order))
    }

    // static async validateOrders(payment: number) {
    //     if (typeof payment !== 'number' || !payment) {
    //         throw (new Err(HttpCode.E400, 'Invalid Payment ID - 1'))
    //     }
    //
    //     let res: IdCheckRes = null
    //     try {
    //         let temp = await OrderController.checkIdExists([payment], PaymentController.repo)
    //         if (temp.index !== -1) {
    //             throw new Err(HttpCode.E400, 'Invalid Payment ID - 2, ' + temp.index)
    //         }
    //         res = temp
    //     } catch (e) {
    //         throw (new Err(HttpCode.E400), 'Invalid Payment ID - 3, ', e)
    //     }
    //
    //     return res
    // }

    static async create(request: Request, response: Response, next: NextFunction) {
        let {price, taxRate, total, orderItems, payment} = request.body

        let order = new Order()
        order.price = price
        order.taxRate = taxRate
        order.total = total
        order.orderItems = orderItems

        try {
            const errors = await validate(order)
            if (errors.length > 0) {
                return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter))
            }

            // use queryBuilder to create a query that finds the orderStatus entity and get one entity from it
            const orderStatus = await getConnection().getRepository(OrderStatus)
                .createQueryBuilder('orderStatus')
                .getOne()
            order.orderStatus = orderStatus

            // when a new order is created, a new payment will be created at the same time with status and type
            // todo: entends paymentController to create payments while creating an order
            const payment = new Payment()
            payment.total = total
            payment.order = order
            const paymentStatus = await getConnection().getRepository(PaymentStatus)
                .createQueryBuilder('paymentStatus')
                .getOne()
            payment.paymentStatus = paymentStatus
            const paymentType = await getConnection().getRepository(PaymentType)
                .createQueryBuilder('paymentType')
                .getOne()

            // order.payment = payment

            // let res = await OrderController.validateOrders(payment)
            // order.payment = res[0].entities[0]
            await getConnection().getRepository(Payment).save(payment)
            await OrderController.repo.save(order)
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, order))
    }

    static async delete(request: Request, response: Response, next: NextFunction) {
        // remove request
        const {orderId} = request.params
        const order = await OrderController.repo.findOne(orderId)

        try {
            if (!order) {
                return response.status(HttpCode.E404).send(new Err(HttpCode.E404, ErrStr.ErrNoObj))
            }

            order.isDelete = true
            order.isActive = false

            const paymentRepo = getConnection().getRepository(Payment)
            // console.log('test1')
            const payment = await paymentRepo.findOne({order: {id: orderId}})
            // console.log('test2')
            if (payment) {
                payment.isDelete = true
                payment.isActive = false
                await paymentRepo.remove(payment)
            }
            // console.log('test3')

            await OrderController.repo.remove(order)
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, order))
    }

    static async update(request: Request, response: Response, next: NextFunction) {
        const {orderId} = request.params

        if (!orderId) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter))
        }

        let order = null
        try {
            order = await OrderController.repo.findOneOrFail(orderId)
        } catch (e) {
            return response.status(HttpCode.E404).send(new Err(HttpCode.E404, ErrStr.ErrStore))
        }

        let {orderItems, price, taxRate, total, shippingAddr, billingAddr} = request.body

        order.orderItems = orderItems
        order.price = price
        order.taxRate = taxRate
        order.total = total
        // order.shippingAddr = shippingAddr
        // order.billingAddr = billingAddr

        const errors = await validate(order)
        if (errors.length > 0) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter, errors))
        }

        try {
            await OrderController.repo.save(order)
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK))
    }

    // static async ranged(request: Request, response: Response, next: NextFunction) {
    //     const current = new Date
    //     const threeMonthsBack = new Date(current.setMonth(current.getMonth() - 3))
    //     let orders = []
    //     try {
    //         // console.log(request.params)
    //         orders = await OrderController.repo.find({
    //             where: {
    //                 createdAt: Between(
    //                     current,
    //                     threeMonthsBack
    //                 )
    //             }
    //         })
    //     } catch (e) {
    //         return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
    //     }
    //
    //     return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, orders))
    // }
}
