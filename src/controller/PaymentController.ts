import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Payment} from "../entity/Payment";
import {Err, ErrStr, HttpCode} from "../helper/Err";
import {validate} from "class-validator";
import {IdCheckController, IdCheckRes} from "./IdCheckController";

export class PaymentController extends IdCheckController {

    public static get repo() {
        return getRepository(Payment)
    }

    static async all(request: Request, response: Response, next: NextFunction) {
        let payments = []
        try {
            payments = await PaymentController.repo.find()
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }
        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, payments))
    }


    static async one(request: Request, response: Response, next: NextFunction) {
        const {paymentId} = request.params
        if (!paymentId) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter))
        }

        let payment = null

        try {
            payment = await PaymentController.repo.findOneOrFail(paymentId)
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, payment))
    }

    static async validateOrders(order: number) {
        if (typeof order !== 'number' || !order) {
            throw (new Err(HttpCode.E400, 'Invalid Payment ID - 1'))
        }

        let res: IdCheckRes = null
        try {
            let temp = await PaymentController.checkIdExists([order], PaymentController.repo)
            if (temp.index !== -1) {
                throw new Err(HttpCode.E400, 'Invalid Payment ID - 2, ' + temp.index)
            }
            res = temp
        } catch (e) {
            throw (new Err(HttpCode.E400), 'Invalid Payment ID - 3, ', e)
        }

        return res
    }

    static async create(request: Request, response: Response, next: NextFunction) {
        let {total, order} = request.body

        let payment = new Payment()
        payment.total = total

        try {
            const errors = await validate(payment)
            if (errors.length > 0) {
                return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter))
            }

            let res = await PaymentController.validateOrders(order)
            order.payment = res[0].entities[0]

            await PaymentController.repo.save(payment)
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }
        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, payment))
    }

    static async delete(request: Request, response: Response, next: NextFunction) {
        const {paymentId} = request.params
        const payment = await PaymentController.repo.findOne(paymentId)
        try {
            if (!payment) {
                return response.status(HttpCode.E404).send(new Err(HttpCode.E404, ErrStr.ErrNoObj))
            }

            await PaymentController.repo.remove(payment)
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK, payment))
    }

    static async update(request: Request, response: Response, next: NextFunction) {
        const {paymentId} = request.params

        if (!paymentId) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter))
        }

        let payment = null
        try {
            payment = await PaymentController.repo.findOneOrFail(paymentId)
        } catch (e) {
            return response.status(HttpCode.E404).send(new Err(HttpCode.E404, ErrStr.ErrStore))
        }

        let {total, paymentType, paymentStatus} = request.body

        payment.total = total
        // payment.paymentType = paymentType
        // payment.paymentStatus = paymentStatus

        const errors = await validate(payment)
        if (errors.length > 0) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrMissingParameter, errors))
        }

        try {
            await PaymentController.repo.save(payment)
        } catch (e) {
            return response.status(HttpCode.E400).send(new Err(HttpCode.E400, ErrStr.ErrStore, e))
        }

        return response.status(HttpCode.E200).send(new Err(HttpCode.E200, ErrStr.OK))
    }
}
